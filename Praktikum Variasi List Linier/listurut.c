/* 
	NIM : 13515143
	Nama : Agus Gunawan
	Tanggal : 27 Oktober 2016
	Topik 	: Variasi List Linier
*/

#include <stdio.h>
#include "listdp.h"

address SearchPrec (List L, infotype X) {
/* Mengirimkan address elemen sebelum elemen yang nilainya>=X */
/* Mencari apakah ada elemen list dengan Info(P)>=X */
/* Jika ada, mengirimkan address Prec, dengan Next(Prec)=P dan Info(P)>=X pertama. */
/* Jika tidak ada, mengirimkan Nil */
/* Jika P adalah elemen pertama, maka Prec=Nil */
	address Prec,P;
	boolean found = false;
	if (IsEmpty(L)) {
		return Nil;
	} else {
		Prec = Nil;
		P = First(L);
		while ( P!= Nil && !found ) {
			if (Info(P) >= X) {
				found = true;
			} else {
				Prec = P;
				P = Next(P);
			}
		}
		if (found) {
			return Prec;
		} else {
			return Nil;
		}
	}
}
	
void InsertUrut (List *L, infotype X) {
/* Menambahkan elemen pada list L tanpa mengganggu keterurutan L.
Manfaatkan fungsi SearchPrec. */
/* I.S. X terdefinisi. L terdefinisi, mungkin kosong.
Jika tidak kosong, L terurut membesar. */
/* F.S. X dialokasi. Jika alokasi berhasil, maka X dimasukkan sebagai salah
satu elemen L tanpa mengganggu keterurutan L. Insert elemen baru
dapat dilakukan di awal, di tengah, maupun di akhir.
Jika alokasi gagal, L tetap. */
	address Prec,P;
	address Last;
	if (IsEmpty(*L)) {
		P = Alokasi(X);
		if (P!=Nil) {
			InsertFirst(L,P);
		}
	} else if (SearchPrec(*L,X) == Nil ) {
		P = Alokasi(X);
		Last = First(*L);
		if (P != Nil ) {
			if (Info(Last) >= X) {
				InsertFirst(L,P);
			} else {
				InsertLast(L,P);
			}
		}
	} else {
		P = Alokasi(X);
		Prec = SearchPrec(*L,X);
		InsertAfter(L,P,Prec);
	}
}
infotype Max (List L) {
/* Menghasilkan nilai terbesar dari elemen list L. L tidak kosong. */
	int maks;
	address P;
	P = First(L);
	maks = Info(P);
	while (P!=Nil) {
		if (Info(P) > maks) {
			maks = Info(P);
		}
		P = Next(P);
	}
	return maks;
}
infotype Min (List L) {
/* Menghasilkan nilai terkecil dari elemen list L. L tidak kosong. */
	int mins;
	address P;
	P = First(L);
	mins = Info(P);
	while (P != Nil) {
		if (Info(P) < mins) {
			mins = Info(P);
		}
		P = Next(P);
	}
	return mins;
}
float Average (List L) {	
/* Menghasilkan nilai rata-rata elemen L. L tidak kosong. */
	float sum = 0;
	address P;
	int n = 0;
	P = First(L);
	while (P != Nil ) {
		sum += Info(P);
		n++;
		P = Next(P);
	}
	return (sum/n);
}

int main() {
	List L;
	infotype X;
	scanf("%d",&X);
	CreateEmpty(&L);
	while (X != -9999 ) {
		InsertUrut(&L,X);
		scanf("%d",&X);
	}
	if (!IsEmpty(L)) {
		printf("%d\n",Max(L));
		printf("%d\n",Min(L));
		printf("%.2f\n",Average(L));
		PrintForward(L);
		printf("\n");
		PrintBackward(L);
		printf("\n");
	} else {
		PrintForward(L);
		printf("\n");
	}	
	
	return 0;
}
