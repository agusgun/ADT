/* 
	NIM : 13515143
	Nama : Agus Gunawan
	Tanggal : 27 Oktober 2016
	Topik 	: Variasi List Linier
*/
#include <stdio.h>
#include "listsirkuler.h"

float Average(List L) {
	address P;
	float sum = 0;
	int n = 0;
	P = First(L);
	while (Next(P) != First(L)) {
		sum+= Info(P);
		n++;
		P = Next(P);
	}
	sum += Info(P);
	n++;
	return (sum/n);
}
int main() {
	List L;
	address Last;
	int bil;
	int Q;
	char proses;
	
	CreateEmpty(&L);
	
	scanf("%d",&Q);
	while (Q<=0) {
		scanf("%d",&Q);
	}
	scanf("\n%c",&proses);
	while (proses != 'F') {
		if (proses == 'A') {
			scanf("%d",&bil);
			if (bil>0) {
				InsVFirst(&L,bil);
			}
		} else if (proses == 'D') {
			if (IsEmpty(L)) {
				printf("List kosong\n");
			} else {
				Last = First(L);
				while (Next(Last) != First(L)) {
					Last = Next(Last);
				}
				bil = Info(Last);
				if (bil>Q) {
					printf("%d\n",Q);
					Info(Last) = Info(Last) - Q;
					First(L) = Last;
				} else {
					DelVLast(&L,&bil);
					printf("%d\n",bil);
				}
			}
		} else {
			printf("Kode salah\n"); 
		}	
		scanf("\n%c",&proses);
	}
	if (IsEmpty(L)) {
		printf("Proses selesai\n");
	} else {
		printf("%.2f\n",Average(L));
	}
	return 0;
}
