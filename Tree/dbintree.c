#include "bintree.h"
#include <stdio.h>

int main() {
  BinTree P1,P2,P3,L1,R1,L2,R2;
  List L;
  int X;
  P3 = Tree(10,AlokNode(11),AlokNode(12));
  L1 = AlokNode(6);
  R1 = AlokNode(7);
  P1 = Tree(5,L1,R1);
  R2 = AlokNode(3);
  P2 = Tree(1,P1,P3);

  PrintTree(P2,2);
  L = MakeListPreorder(P2);
  return 0;
}
