#include "map.h"
#include <stdlib.h>
#include <stdio.h>


/* PROTOTYPE */
/****************** TEST LIST KOSONG ******************/
boolean IsEmpty (List L) {
/* Mengirim true jika list kosong. Lihat definisi di atas. */
  return (First(L) == Nil);
}
/****************** PEMBUATAN LIST KOSONG ******************/
void CreateEmptyMap (List *L) {
/* I.S. L sembarang  */
/* F.S. Terbentuk list kosong. Lihat definisi di atas. */
  First(*L) = Nil;
}
/****************** Manajemen Memori ******************/
address Alokasi (infotypemap X) {
/* Mengirimkan address hasil alokasi sebuah elemen */
/* Jika alokasi berhasil, maka address tidak nil. */
/* Misalnya: menghasilkan P, maka Info(P)=X, Right(P)=Nil, Left(P)=Nil, Up(P)=Nil, Down(P)=Nil */
/* Jika alokasi gagal, mengirimkan Nil. */
  address P;
  int i;
  P = (address) malloc (sizeof(ElmtList));
  if (P != Nil) {
    Info(P) = X;
    for (i = 1; i<=MaxNeighbor; i++) {
      Neighbor(P)[i] = Nil;
    }
    Visited(P) = false;
  }
  return P;
}
void Dealokasi (address P) {
/* I.S. P terdefinisi */
/* F.S. P dikembalikan ke sistem */
/* Melakukan dealokasi/pengembalian address P */
  free(P);
}
/****************** PRIMITIF BERDASARKAN NILAI ******************/
/*** PENAMBAHAN ELEMEN ***/
void SetFirst (List *L, infotypemap X) {
/* I.S. L mungkin kosong */
/* F.S. Melakukan alokasi sebuah elemen dan */
/* menambahkan elemen pertama dengan nilai X jika alokasi berhasil */
  address P;
  P = Alokasi(X);
  if (P!=Nil) {
    First(*L) = P;
  }
}

void Set(address P1, address *P2, int N) {
/* I.S. L tidak kosong */
/* F.S. Melakukan alokasi sebuah elemen dan */
/* menambahkan elemen X di kiri list sehingga Neighbor(*P2)[N] = P1 */
  Neighbor(*P2)[N] = P1;
}
