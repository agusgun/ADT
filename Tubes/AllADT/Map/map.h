/* File : listdp.h */
/* ADT List dengan Double Pointer */
/* Representasi berkait dengan address adalah pointer */
/* infotypemap adalah matriks dan gate */

#ifndef map_H
#define map_H

#include "boolean.h"
#include "point.h"
#include "matriks.h"
#define Nil NULL
#define MaxNeighbor 4
#define MaxMap 10
/* Definisi Type Data */
typedef struct {
	int Arah;
	int NomorMap;
} Pilih;
typedef struct {
	Pilih Pilihan[50];
	int Length;
} Koneksi;
typedef struct {
	POINT PositionEnemy[50];
	int Length;
} POINTOFENEMY;
typedef struct{
	MATRIKS MAP;
	POINT GateArrived[MaxNeighbor+1];
	POINT GateDepart[MaxNeighbor+1];
} infotypemap;
typedef struct tElmtlist *address;
typedef struct tElmtlist {
	infotypemap info;
	address Neighbor[MaxNeighbor+1];
	boolean visited;
} ElmtList;

typedef struct {
	address First;
} List;

/* Definisi list : */
/* List kosong : First(L) = Nil  */
/* Setiap elemen dengan address P dapat diacu Info(P), Left(P), Right(P), Down(P), Up(P) */
/* Elemen terakhir list: Last(L) */

/* Notasi Akses */
#define Info(P) 	(P)->info
#define Neighbor(P) (P)->Neighbor
#define Visited(P) (P)->visited
#define Map(P) 		(Info(P)).MAP
#define GateArrived(X)		X.GateArrived
#define GateDepart(X)		X.GateDepart
#define First(L)	((L).First)
#define Pilihan(K) (K).Pilihan
#define LengthKoneksi(K) (K).Length
#define LengthPOINTOFENEMY(E) (E).Length
#define PositionEnemy(E) (E).PositionEnemy
/* PROTOTYPE */
/****************** TEST LIST KOSONG ******************/
boolean IsEmpty (List L);
/* Mengirim true jika list kosong. Lihat definisi di atas. */

/****************** PEMBUATAN LIST KOSONG ******************/
void CreateEmptyMap (List *L);
/* I.S. L sembarang  */
/* F.S. Terbentuk list kosong. Lihat definisi di atas. */

/****************** Manajemen Memori ******************/
address Alokasi (infotypemap X);
/* Mengirimkan address hasil alokasi sebuah elemen */
/* Jika alokasi berhasil, maka address tidak nil. */
/* Misalnya: menghasilkan P, maka Info(P)=X, Right(P)=Nil, Left(P)=Nil, Up(P)=Nil, Down(P)=Nil */
/* Jika alokasi gagal, mengirimkan Nil. */
void Dealokasi (address P);
/* I.S. P terdefinisi */
/* F.S. P dikembalikan ke sistem */
/* Melakukan dealokasi/pengembalian address P */


/****************** PRIMITIF BERDASARKAN NILAI ******************/
/*** PENAMBAHAN ELEMEN ***/
void Set(address P1, address *P2, int N);
/* I.S. L tidak kosong */
/* F.S. Melakukan alokasi sebuah elemen dan */
/* menambahkan elemen X di kiri list sehingga Neighbor(*P2)[N] = P1 */

void SetFirst (List *L, infotypemap X);
/* I.S. L mungkin kosong */
/* F.S. Melakukan alokasi sebuah elemen dan */
/* menambahkan elemen pertama dengan nilai X jika alokasi berhasil */


#endif
