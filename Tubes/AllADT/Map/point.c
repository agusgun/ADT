/*
	Nama : Agus Gunawan
	NIM : 13515143
	Topik : Pra Praktikum
	Deskripsi : Implementasi ADT Point
*/

#include <stdio.h>
#include "point.h"
#include <math.h>

/* *** DEFINISI PROTOTIPE PRIMITIF *** */
/* *** Konstruktor membentuk POINT *** */
POINT MakePOINT (int X, int Y){
/* Membentuk sebuah POINT dari komponen-komponennya */
	POINT p;
	Absis(p) = X;
	Ordinat(p) = Y;
	return p;
}

/* *** KELOMPOK Interaksi dengan I/O device, BACA/TULIS  *** */
void BacaPOINT (POINT * P){
/* Membaca nilai absis dan ordinat dari keyboard dan membentuk
   POINT P berdasarkan dari nilai absis dan ordinat tersebut */
/* Komponen X dan Y dibaca dalam 1 baris, dipisahkan 1 buah spasi */
/* Contoh: 1 2
   akan membentuk POINT <1,2> */
/* I.S. Sembarang */
/* F.S. P terdefinisi */
	int x,y;
	scanf("%f %f",&x,&y);
	*P = MakePOINT(x,y);
}
void TulisPOINT (POINT P){
/* Nilai P ditulis ke layar dengan format "(X,Y)"
   tanpa spasi, enter, atau karakter lain di depan, belakang,
   atau di antaranya */
/* I.S. P terdefinisi */
/* F.S. P tertulis di layar dengan format "(X,Y)" */
   printf(("(%.2f,%.2f)"),Absis(P),Ordinat(P));
}

/* *** Kelompok operasi relasional terhadap POINT *** */
boolean EQ (POINT P1, POINT P2){
/* Mengirimkan true jika P1 = P2 : absis dan ordinatnya sama */
	return ((Absis(P1)==Absis(P2))&&((Ordinat(P1)==Ordinat(P2))));
}
boolean NEQ (POINT P1, POINT P2){
/* Mengirimkan true jika P1 tidak sama dengan P2 */
	return !EQ(P1,P2);
}

/* *** Kelompok menentukan di mana P berada *** */
boolean IsOrigin (POINT P){
/* Menghasilkan true jika P adalah titik origin */
	if ((Absis(P)==0)&&(Ordinat(P)==0)){
		return true;
	} else
		return false;
}
boolean IsOnSbX (POINT P){
/* Menghasilkan true jika P terletak Pada sumbu X */
	if (Ordinat(P)==0) {
		return true;
	} else
		return false;
}
boolean IsOnSbY (POINT P){
/* Menghasilkan true jika P terletak pada sumbu Y */
	if (Absis(P)==0) {
		return true;
	} else
		return false;
}
int Kuadran (POINT P){
/* Menghasilkan kuadran dari P: 1, 2, 3, atau 4 */
/* Prekondisi : P bukan titik origin, */
/*              dan P tidak terletak di salah satu sumbu */
	if ((Absis(P)>0)&&(Ordinat(P)>0)) {
		return 1;
	} else if ((Absis(P)<0)&&(Ordinat(P)>0)) {
		return 2;
	} else if ((Absis(P)<0)&&(Ordinat(P)<0)) {
		return 3;
	} else {
		return 4;
	}
}

/* *** KELOMPOK OPERASI LAIN TERHADAP TYPE *** */
POINT NextX (POINT P){
/* Mengirim salinan P dengan absis ditambah satu */
	return (MakePOINT(Absis(P)+1,Ordinat(P)));
}
POINT NextY (POINT P){
/* Mengirim salinan P dengan ordinat ditambah satu */
	return (MakePOINT(Absis(P),Ordinat(P)+1));
}
POINT PlusDelta (POINT P, int deltaX, int deltaY){
/* Mengirim salinan P yang absisnya adalah Absis(P) + deltaX dan ordinatnya adalah Ordinat(P) + deltaY */
	return (MakePOINT((Absis(P)+deltaX),(Ordinat(P)+deltaY)));
}
