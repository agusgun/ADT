#include <stdio.h>
#include "mesinkar.h"
#include "map.h"
#include "mesinkata.h"
#include <stdlib.h>
#include <time.h>

void HapusGate(address *P) {
/*  I.S : P terdefinisi, P dapat berisi nilai Nil
    F.S : Setiap node yang tidak memiliki koneksi, gate akan dihapus
    Setiap node akan dikunjungi dan akan dicek apakah up, down, left, rightnya
    berisi nilai address, jika tidak maka gate akan dihapus, jika berisi suatu
    address maka node tersebut akan dikunjungi untuk dicek lagi jika belum pernah
    dikunjungi sebelumnya
*/
  int i;
  if (*P == Nil) {
    //Do Nothing
  } else if (Visited(*P)) {
    //Do Nothing
  } else {
    Visited(*P) = true;
    i = 1;
    while (i<=MaxNeighbor) {
      HapusGate(&Neighbor(*P)[i]);
      i++;
    }
    i = 1;
    while (i <= MaxNeighbor) {
      if (Elmt(Map(*P),Ordinat(GateDepart(Info(*P))[i]),Absis(GateDepart(Info(*P))[i]) ) == '.' && Neighbor(*P)[i] == Nil) {
        Elmt(Map(*P),Ordinat(GateDepart(Info(*P))[i]),Absis(GateDepart(Info(*P))[i]) ) = '#';
      }
      i++;
    }
  }
}
void EmptyGateInfo(infotypemap *X) {
/* I.S : X terdefinisi
   F.S : Nilai yang berhubungan dengan Gate akan diassign menjadi Point(0,0).
*/
  int i = 0;
  while (i<=MaxNeighbor) {
    (*X).GateArrived[i] = MakePOINT(0,0);
    i++;
  }
  i = 0;
  while (i<=MaxNeighbor) {
    (*X).GateDepart[i] = MakePOINT(0,0);
    i++;
  }
}
void SaveEnemyPoint(MATRIKS A[MaxMap+1], POINTOFENEMY *PosEnemy) {
/*
    I.S : A adalah array of area yang terdefinisi dan tidak kosong
    F.S : Setiap posisi musuh di suatu map akan di catat dan di simpan di array of Point PosEnemy
*/
  int index;
  int i,j;
  for (index = 1; index <= MaxMap; index++) {
    LengthPOINTOFENEMY(PosEnemy[index]) = 0;
    for (i = 1; i<= 10; i++) {
      for (j = 1; j<=10; j++) {
        if (Elmt(A[index],i,j) == 'E') {
          LengthPOINTOFENEMY(PosEnemy[index])++;
          PositionEnemy(PosEnemy[index])[LengthPOINTOFENEMY(PosEnemy[index])] = MakePOINT(j,i);
        }
      }
    }
  }
}
void SearchGateCopyMatrix(infotypemap *X,MATRIKS A[MaxMap+1], int arrrandomnumber[MaxMap+1]){
/*
  Mencari setiap koordinat gate dari masing - masing area
  I.S : X berisi array of infotypemap yang kosong, A berisi array of area dimana area adalah matrix of char
        dan arrrandomnumber berisi bilangan random yang unik dan telah berisi nilai
  F.S : X berisi array of infotypemap yang setiap elemennya berisi area A[index] dimana setiap index diambil
        dari arrandomnumber[i]
*/
  int i,j,k,n;
  for (k = 1; k<=MaxMap; k++) {
    CopyMATRIKS(A[arrrandomnumber[k]],&X[k].MAP);
    EmptyGateInfo(&X[k]);
    n = 0;
    //Search All Gate at Top
    i = 1; j = 1;
    while (j<=10) {
      if (Elmt(A[arrrandomnumber[k]],i,j) == '.') {
        n++;
        X[k].GateDepart[n] = MakePOINT(j,i);
      }
      j++;
    }
    //Search All Gate at Right
    i = 1; j = 10;
    while (i<=10) {
      if (Elmt(A[arrrandomnumber[k]],i,j) == '.') {
        n++;
        X[k].GateDepart[n] = MakePOINT(j,i);
      }
      i++;
    }
    //Search All Gate at Down
    i = 10; j = 10;
    while (j >= 1) {
      if (Elmt(A[arrrandomnumber[k]],i,j) == '.') {
        n++;
        X[k].GateDepart[n] = MakePOINT(j,i);
      }
      j--;
    }
    //Search All Gate at Left
    i = 10; j = 1;
    while (i >= 1) {
      if (Elmt(A[arrrandomnumber[k]],i,j) == '.') {
        n++;
        X[k].GateDepart[n] = MakePOINT(j,i);
      }
      i--;
    }
  }
}

void Baca_Area(MATRIKS *A) {
/*
  I.S : Matriks A terdefinisi
  F.S : Matriks A berisi semua area yang telah dibaca dari file eksternal
*/
  int kol,brs;
  int i;
  START("map.txt");
  for (i=1; i<=MaxMap; i++) {
    ADVKATA();
    ADV();
    MakeMATRIKS(10,10,&(A[i]));
    brs = 1;
    kol = 1;
    while (CC != '*' && CC!=MARK) {
      if (CC == '\n') {
        brs++;
        kol = 1;
      } else {
        Elmt(A[i],brs,kol) = CC;
        kol++;
      }
      ADV();
    }
  }
  ADVKATA();
}
void LoadMap(List *L, MATRIKS A[MaxMap+1], Koneksi Connection) {
/*
  I.S : List L kosong, Connection berisi nilai koneksi, MATRIKS A[11] akan berisi semua area yang dibaca dari file eksternal
  F.S : Akan terbentuk map yang sama dari parameter input connection
  Koneksi Connection akan dibaca dan dari koneksi tersebut akan digenerate map yang sama dengan
  yang tertulis di koneksi
*/
  int i;
  int j,k;
  infotypemap X[MaxMap+1];
  int urutan[MaxMap+1];
  address P1,P2;
  CreateEmptyMap(L);
  j = 0;
  Baca_Area(A);
  for (i = 1; i<= LengthKoneksi(Connection); i++) {
    if (Pilihan(Connection)[i].NomorMap > 0) {
      j++;
      urutan[j] = Pilihan(Connection)[i].NomorMap;
    }
  }
  j = 0;
  SearchGateCopyMatrix(X,A,urutan);

  for ( i = 1; i<=LengthKoneksi(Connection); i++) {
    if (Pilihan(Connection)[i].Arah == 0) {
      j++;
      SetFirst(L,X[j]);
      P2 = First(*L);
    } else if (Pilihan(Connection)[i].Arah >= 1 && Pilihan(Connection)[i].Arah <= MaxNeighbor) {
      j++;
      P1 = Alokasi(X[j]);
      Set(P1,&P2,i);
      k = i + 2;
      if (k > MaxNeighbor) {
        k = k - MaxNeighbor;
      }
      GateArrived(Info(P2))[i] = GateDepart(Info(P1))[k];
      Set(P2,&P1,k);
      GateArrived(Info(P1))[k] = GateDepart(Info(P1))[i];
    } else {
      k = k - 4;
      P2 = Neighbor(P2)[k];
    }
  }
  P2 = First(*L);
  HapusGate(&P2);
}
void PlaceBoss(MATRIKS *M) {
/* Menaruh bos di area paling jauh
   I.S : M terdefinisi
   F.S : Matriks M dengan nilai titik random akan diganti menjadi karakter 'B'
*/
  int x,y;
  x = rand() % 10;
  x++;
  y = rand() % 10;
  y++;
  while (x == 10 || x == 1 || Elmt(*M,y,x) == '#' || y == 1 || y == 10 || Elmt(*M,y,x) == 'E' || Elmt(*M,y,x) == 'M') {
    x = rand() % 10;
    x++;
    y = rand() % 10;
    y++;
  }
  Elmt(*M,y,x) = 'B';
}
void init_list(List *L, MATRIKS A[11], Koneksi *Con) {
/*  I.S : List L memiliki nilai Nil
    F.S : Terbentuk peta yang saling terhubung dari yang setiap node memili area
          yang diambil dari array of matriks A
*/
  address P1,P2;
  int i;
  int j,k;
  int randomnumber,randomindex;
  int arrrandomnumber[11];
  boolean sudahdikunjungin[11];
  POINT PosB;

  infotypemap X[11];
  boolean sudahmap[11];
  boolean sudahindex[5];

  srand(time(NULL));
  //Generate array of random number
  i = 1;
  while (i<=10) {
    randomnumber = rand() % 10;
    randomnumber++;
    while (sudahmap[randomnumber]) {
      randomnumber = rand() % 10;
      randomnumber++;
    }
    sudahmap[randomnumber] = true;
    arrrandomnumber[i] = randomnumber;
    i++;
  }
  SearchGateCopyMatrix(X,A,arrrandomnumber);

  PlaceBoss(&(X[10].MAP));

  LengthKoneksi(*Con) = 0;

  LengthKoneksi(*Con)++;
  Pilihan(*Con)[1].Arah = 0;
  Pilihan(*Con)[1].NomorMap = arrrandomnumber[1];
  SetFirst(L,X[1]);
  P2 = First(*L);
  i = 2;
  while (i <= MaxMap) {
    randomindex = rand() % (MaxNeighbor * 2);
    randomindex++;
    if (randomindex <= MaxNeighbor && Neighbor(P2)[randomindex] == Nil) {
      LengthKoneksi(*Con)++;
      Pilihan(*Con)[LengthKoneksi(*Con)].Arah = randomindex;
      Pilihan(*Con)[LengthKoneksi(*Con)].NomorMap = arrrandomnumber[i];

      P1 = Alokasi(X[i]);
      Set(P1,&P2,randomindex);
      k = randomindex + 2;
      if (k > MaxNeighbor) {
        k = k - MaxNeighbor;
      }
      GateArrived(Info(P2))[randomindex] = GateDepart(Info(P1))[k];
      Set(P2,&P1,k);
      GateArrived(Info(P1))[k] = GateDepart(Info(P2))[randomindex];

      i++;
    } else if (randomindex > 4 && Neighbor(P2)[randomindex - MaxNeighbor] != Nil) {
      LengthKoneksi(*Con)++;
      Pilihan(*Con)[LengthKoneksi(*Con)].Arah = randomindex;
      Pilihan(*Con)[LengthKoneksi(*Con)].NomorMap = 0;

      P2 = Neighbor(P2)[randomindex - MaxNeighbor];
    }
  }

  //buat hapus semua Gate yang gak ada jalan
  P2 = First(*L);
  HapusGate(&P2);
}
POINT Random_Pos_Player(MATRIKS M) {
/*
  Menghasilkan Posisi Random Player saat di awal permainan dengan beberapa constraint
  I.S : Matriks M terdefinisi dan tidak kosong
  F.S : Menghasilkan posisi random untuk player dimana di point tersebut tidak terdapat simbol 'E', simbol'M
        ataupun simbol '#'
*/
  int x,y;
  x = rand() % 10;
  x++;
  y = rand() % 10;
  y++;
  while (x == 10 || x == 1 || Elmt(M,y,x) == '#' || y == 1 || y == 10 || Elmt(M,y,x) == 'E' || Elmt(M,y,x) == 'M') {
    x = rand() % 10;
    x++;
    y = rand() % 10;
    y++;
  }
  return (MakePOINT(x,y));
}
void Penjelajahan() {
/*
  Semua yang berkaitan dengan penjelajahan player di area dan berpindah antar area
*/
  POINT POS_Player;
  int brs,kol;
  char command;
  int i,j;
  Kata End,GU,GD,GL,GR;
  List L;
  address P;
  POINTOFENEMY E[11];
  MATRIKS A[11];
  End.Length = 3;
  End.TabKata[1] = 'E';
  End.TabKata[2] = 'N';
  End.TabKata[3] = 'D';
  GU.Length = 2;
  GU.TabKata[1] = 'G';
  GU.TabKata[2] = 'U';
  GD.Length = 2;
  GD.TabKata[1] = 'G';
  GD.TabKata[2] = 'D';
  GL.Length = 2;
  GL.TabKata[1] = 'G';
  GL.TabKata[2] = 'L';
  GR.Length = 2;
  GR.TabKata[1] = 'G';
  GR.TabKata[2] = 'R';



  CreateEmptyMap(&L);
  Baca_Area(A);
  SaveEnemyPoint(A,E);
  Koneksi Con;
  init_list(&L, A, &Con);

  P = First(L);
  POS_Player = Random_Pos_Player(Map(P));

  //system("clear");
  TulisMATRIKS(Map(P),Absis(POS_Player),Ordinat(POS_Player));
  printf("Input : ");
  BACAKATAUI();
  while (!IsKataSama(CKata,End)) {
    if (IsKataSama(CKata,GD)) {
      if (Elmt(Map(P),Ordinat(POS_Player) + 1, Absis(POS_Player)) != '#') {
        if (Ordinat(POS_Player)+1 > GetLastIdxBrs(Map(P)) ) {
          if ( Neighbor(P)[3] != Nil) {
            Absis(POS_Player) = Absis(GateArrived(Info(P))[3]);
            Ordinat(POS_Player) = Ordinat(GateArrived(Info(P))[3]);
            P = Neighbor(P)[3];
          }
        } else {
          if(Elmt(Map(P),Ordinat(POS_Player) + 1, Absis(POS_Player)) == 'E' || Elmt(Map(P),Ordinat(POS_Player) + 1, Absis(POS_Player)) == 'M')  {
              Elmt(Map(P),Ordinat(POS_Player) + 1, Absis(POS_Player)) = '.';
          }
          Ordinat(POS_Player)++;
        }
      }
    } else if (IsKataSama(CKata,GL)) {
      if (Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player)-1) != '#') {
        if (Absis(POS_Player)-1 < GetFirstIdxKol(Map(P)) ) {
          if ( Neighbor(P)[4] != Nil) {
            Absis(POS_Player) = Absis(GateArrived(Info(P))[4]);
            Ordinat(POS_Player) = Ordinat(GateArrived(Info(P))[4]);
            P = Neighbor(P)[4];
          }
        } else {
          if(Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player) - 1) == 'E' || Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player) - 1) == 'M')  {
              Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player) - 1) = '.';
          }
          Absis(POS_Player)--;
        }
      }
    } else if (IsKataSama(CKata,GR)) {
      if (Elmt(Map(P),Ordinat(POS_Player),Absis(POS_Player)+1) != '#') {
        if (Absis(POS_Player)+1 > GetLastIdxKol(Map(P)) ) {
          if ( Neighbor(P)[2] != Nil) {
            Absis(POS_Player) = Absis(GateArrived(Info(P))[2]);
            Ordinat(POS_Player) = Ordinat(GateArrived(Info(P))[2]);
            P = Neighbor(P)[2];
          }
        } else {
          if(Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player) + 1) == 'E' || Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player) + 1) == 'M')  {
              Elmt(Map(P),Ordinat(POS_Player), Absis(POS_Player) + 1) = '.';
          }
          Absis(POS_Player)++;
        }
      }
    } else if (IsKataSama(CKata,GU)) {
      if (Elmt(Map(P),Ordinat(POS_Player)-1,Absis(POS_Player)) != '#') {
        if (Ordinat(POS_Player)-1 < GetFirstIdxBrs(Map(P)) ) {
          if ( Neighbor(P)[1] != Nil) {
            Absis(POS_Player) = Absis(GateArrived(Info(P))[1]);
            Ordinat(POS_Player) = Ordinat(GateArrived(Info(P))[1]);
            P = Neighbor(P)[1];
          }
        } else {
          if(Elmt(Map(P),Ordinat(POS_Player) - 1, Absis(POS_Player)) == 'E' || Elmt(Map(P),Ordinat(POS_Player) - 1, Absis(POS_Player)) == 'M')  {
              Elmt(Map(P),Ordinat(POS_Player) - 1, Absis(POS_Player)) = '.';
          }
          Ordinat(POS_Player)--;
        }
      }
    } else {
      printf("Command Salah\n");
    }
    if (IsKataSama(CKata,GD) || IsKataSama(CKata,GR) || IsKataSama(CKata,GU) || IsKataSama(CKata,GL)) {
      system("clear");
      TulisMATRIKS(Map(P),Absis(POS_Player),Ordinat(POS_Player));
    }
    printf("Input : ");
    CKata.Length = 0;
    BACAKATAUI();
  }
}
int main() {
  Penjelajahan();

  return 0;
}
