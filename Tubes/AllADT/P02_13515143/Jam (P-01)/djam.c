/* 	
	Nama : Agus Gunawan
	NIM : 13515143
	Topik : Pra Praktikum 
	Deskripsi : Driver ADT Jam
*/

#include "jam.h"
#include <stdio.h>

int main(){
	JAM j1,j2,j3;
	int detik;

	/* Test Baca dan Tulis Jam */
	printf("Masukan Jam ke-1 = ");
	BacaJAM(&j1);
	printf("Masukan Jam ke-2 = ");
	BacaJAM(&j2);

	printf("Jam ke-1 = ");
	TulisJAM(j1);
	printf("\n");
	
	printf("Jam ke-2 = ");
	TulisJAM(j2);
	printf("\n");

	/* Test JamtoDetik dan DetiktoJam */
	printf("Jam ke-1 memiliki jumlah detik = %ld\n",JAMToDetik(j1));
	
	printf("Masukan jumlah detik yang akan dikonversi ke jam = ");
	scanf("%d",&detik);
	j3 = DetikToJAM(detik);
	printf("%d detik akan menjadi ",detik);
	TulisJAM(j3);
	printf(" dalam format Jam\n");

	/* Test JEQ, JNEQ, JLT, JGT */
	if (JNEQ(j1,j2)) {
		printf("Jam ke-1 dan Jam ke-2 tidak sama\n");
	}

	if (JEQ(j1,j2)) {
		printf("Jam ke-1 dan Jam ke-2 sama\n");
	} else if (JLT(j1,j2)) {
		printf("Jumlah detik Jam ke-1 kurang dari Jam ke-2\n");
	} else if (JGT(j1,j2)) {
		printf("Jumlah detik Jam ke-1 lebih dari Jam ke-2\n");
	}
	
	/* Test NextDetik, NextNDetik, PrevDetik, PrevNDetik */
	j1 = NextDetik(j1);
	printf("Jam ke-1 setelah ditambah 1 detik akan menjadi %d:%d:%d\n",Hour(j1),Minute(j1),Second(j1));
	j1 = NextNDetik(j1,5);
	printf("Jam setelah ditambah 5 detik dari sebelumnya akan menjadi %d:%d:%d\n",Hour(j1),Minute(j1),Second(j1));
	j1 = PrevDetik(j1);
	printf("Jam setelah dikurang 1 detik dari sebelumnya akan menjadi %d:%d:%d\n",Hour(j1),Minute(j1),Second(j1));
	j1 = PrevNDetik(j1,10);
	printf("Jam setelah dikurang 10 detik dari sebelumnya akan menjadi %d:%d:%d\n",Hour(j1),Minute(j1),Second(j1));

	/* Test Durasui */
	printf("Durasi antara Jam ke-2 dan Jam ke-1 adalah %ld\n",Durasi(j1,j2));

	return 0;
}