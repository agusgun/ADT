/* 	
	Nama : Agus Gunawan
	NIM : 13515143
	Topik : Pra Praktikum 
	Deskripsi : Driver ADT Point
*/

#include "point.h"
#include <stdio.h>

int main(){
	POINT p1,p2;
	float tambahx,tambahy;

	/* Test Baca Point dan Tulis Point */
	printf("Masukan Point 1 = ");
	BacaPOINT(&p1);
	printf("Masukan Point 2 = ");
	BacaPOINT(&p2);

	printf("P1 = ");
	TulisPOINT(p1);
	printf("\n");
	printf("P2 = ");
	TulisPOINT(p2);
	printf("\n");
	
	/* Test Kesamaan */
	printf("Apakah Point 1 dan 2 sama = ? ");
	if (EQ(p1,p2)){
		printf("Sama EQ\n");
	}
	if (NEQ(p1,p2)){
		printf("Beda NEQ\n");
	}
	
	/* Test Origin */
	printf("Keterangan Posisi Point 1 : ");
	if (IsOrigin(p1)) {
		printf("Point 1 is Origin\n");
	} else 	
	/* Test Pada Sumbu X */
	if (IsOnSbX(p1)){
		printf("Point 1 is on SbX\n");
	} else 	
	/* Test Pada Sumbu Y */
	if (IsOnSbY(p1)){
		printf("Point 1 is on SbY\n");
	}
	
	/* Test Kuadran */
	if (!IsOrigin(p1)){
		printf("Point 1 Kuadran %d\n",Kuadran(p1));
	}

	/* Test NextX dan NextY */
	p1 = NextX(p1);
	printf("Point 1 Next X = %.2f\n",Absis(p1));
	p1 = NextY(p1);
	printf("Point 1 Next Y = %.2f\n",Ordinat(p1));

	/* Test PlusDelta */
	printf("Masukan nilai DeltaX = ");
	scanf("%f",&tambahx);
	printf("masukan nilai DeltaY = ");
	scanf("%f",&tambahy);

	p1 = PlusDelta(p1,tambahx,tambahy);
	printf("PlusDelta DeltaX dan Delta Y = %.2f %.2f\n",Absis(p1),Ordinat(p1));

	/* Test Mirror Of terhadap sumbu X dan Y*/
	p1 = MirrorOf(p1,1);
	printf("Mirror Of Sumbu X = %.2f %.2f\n",Absis(p1),Ordinat(p1));
	p1 = MirrorOf(p1,0);
	printf("Mirror of Sumbu Y = %.2f %.2f\n",Absis(p1),Ordinat(p1));

	/* Test Jarak0 dan Panjang */
	printf("Jarak 0 Point 1 = %.2f\n",Jarak0(p1));
	printf("Jarak Titik 2 dan Titik 1 = %.2f\n",Panjang(p1,p2));

	/* Test Geser, GeserKeSbX, GeserKeSbY, Mirror */
	GeserKeSbX(&p1);
	printf("GeserKeSbX = %.2f %.2f\n",Absis(p1),Ordinat(p1));
	GeserKeSbY(&p1);
	printf("GeserKeSbY = %.2f %.2f\n",Absis(p1),Ordinat(p1));
	Geser(&p1,3,4);
	printf("Geser 3 4 = %.2f %.2f\n",Absis(p1),Ordinat(p1));
	
	/* Test Mirror dan Putar */
	Mirror(&p1,1);
	printf("Mirror Sumbu X = %.2f %.2f\n",Absis(p1),Ordinat(p1));
	Mirror(&p1,0);
	printf("Mirror Sumbu Y = %.2f %.2f\n",Absis(p1),Ordinat(p1));
	Putar(&p1,90);
	printf("Titik Diputar 90 derajat = %.2f %.2f\n",Absis(p1),Ordinat(p1));

	return 0;
}