/*
	Nama : Agus Gunawan
	NIM : 13515143
	Nama File : array.c
	Topik : Pra Praktikum 3
*/

#include "array.h"
#include <stdio.h>

/* ********** KONSTRUKTOR ********** */
/* Konstruktor : create tabel kosong  */
void MakeEmpty (TabInt * T){
/* I.S. T sembarang */
/* F.S. Terbentuk tabel T kosong dengan kapasitas IdxMax-IdxMin+1 */
	Neff(*T) = 0;
}
/* ********** SELEKTOR (TAMBAHAN) ********** */
/* *** Banyaknya elemen *** */
int NbElmt (TabInt T){
/* Mengirimkan banyaknya elemen efektif tabel */	
/* Mengirimkan nol jika tabel kosong */
	return Neff(T);
}
/* *** Daya tampung container *** */
int MaxNbEl (TabInt T){
/* Mengirimkan maksimum elemen yang dapat ditampung oleh tabel */
	return IdxMax;
}
/* *** Selektor INDEKS *** */
IdxType GetFirstIdx (TabInt T){
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks elemen T pertama */
	return IdxMin;
}
IdxType GetLastIdx (TabInt T){
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks elemen T terakhir */
	return Neff(T);
}
/* ********** Test Indeks yang valid ********** */
boolean IsIdxValid (TabInt T, IdxType i){
/* Mengirimkan true jika i adalah indeks yang valid utk ukuran tabel */
/* yaitu antara indeks yang terdefinisi utk container*/
	return ( (i>=IdxMin) && (i<=IdxMax) );
}
boolean IsIdxEff (TabInt T, IdxType i) {
/* Mengirimkan true jika i adalah indeks yang terdefinisi utk tabel */
/* yaitu antara FirstIdx(T)..LastIdx(T) */
	return ( (i>=IdxMin) &&	(i<=Neff(T)) );
}
/* ********** TEST KOSONG/PENUH ********** */
/* *** Test tabel kosong *** */
boolean IsEmpty (TabInt T){
/* Mengirimkan true jika tabel T kosong, mengirimkan false jika tidak */
	return ( Neff(T) == 0 );
}
/* *** Test tabel penuh *** */
boolean IsFull (TabInt T){
/* Mengirimkan true jika tabel T penuh, mengirimkan false jika tidak */
	return ( Neff(T) == IdxMax );
}
/* ********** BACA dan TULIS dengan INPUT/OUTPUT device ********** */
/* *** Mendefinisikan isi tabel dari pembacaan *** */
void BacaIsi (TabInt * T){
/* I.S. T sembarang */
/* F.S. Tabel T terdefinisi */
/* Proses : membaca banyaknya elemen T dan mengisi nilainya */
/* 1. Baca banyaknya elemen diakhiri enter, misalnya N */
/*    Pembacaan diulangi sampai didapat N yang benar yaitu 0 <= N <= MaxNbEl(T) */
/*    Jika N tidak valid, tidak diberikan pesan kesalahan */
/* 2. Jika 0 < N <= MaxNbEl(T); Lakukan N kali: Baca elemen mulai dari indeks 
      IdxMin satu per satu diakhiri enter */
/*    Jika N = 0; hanya terbentuk T kosong */
	int N,i;
	int bil;
	scanf("%d",&N);
	while ( (N<0) || (N>IdxMax) ) {
		scanf( "%d",&N );
	}
	MakeEmpty(T);
	if ( N != 0 ) {
		for ( i = 1; i <= N; i++ ) {
			scanf("%d", &bil );
			Elmt(*T,i) = bil;
		}	
	}
	Neff(*T) = N;
}
void BacaIsiTab (TabInt * T){
/* I.S. T sembarang */
/* F.S. Tabel T terdefinisi */
/* Proses : membaca elemen T sampai dimasukkan nilai -9999 */
/* Dibaca elemen satu per satu dan disimpan mulai dari IdxMin */
/* Pembacaan dihentikan jika pengguna memasukkan nilai -9999 */
/* Jika dari pertama dimasukkan nilai -9999 maka terbentuk T kosong */
	int bil;
	scanf("%d", &bil);
	MakeEmpty(T);
	while ( (bil != -9999) && (Neff(*T)<IdxMax)) {
		Neff(*T)++;
		Elmt(*T, Neff(*T)) = bil;
		scanf("%d", &bil);
	}
}
void TulisIsi (TabInt T){
/* Proses : Menuliskan isi tabel dengan traversal */
/* I.S. T boleh kosong */
/* F.S. Jika T tidak kosong : indeks dan elemen tabel ditulis berderet ke bawah */
/*      Jika T kosong : Hanya menulis "Tabel kosong" */
/* Contoh: Jika isi Tabel: [1, 20, 30, 50]
   Maka tercetak di layar:
   [1]1
   [2]20
   [3]30
   [4]50
*/
	int i;
	if ( Neff(T)==0 ) {
		printf("Tabel kosong\n");
	} else {
		for ( i=1 ; i <= Neff(T); i++ ) {
			printf( "[%d]%d\n",i,Elmt(T,i) );
		}
	}
}
void TulisIsiTab (TabInt T){
/* Proses : Menuliskan isi tabel dengan traversal, tabel ditulis di antara kurung siku; 
   antara dua elemen dipisahkan dengan separator "koma", tanpa tambahan karakter di depan,
   di tengah, atau di belakang, termasuk spasi dan enter */
/* I.S. T boleh kosong */
/* F.S. Jika T tidak kosong: [e1,e2,...,en] */
/* Contoh : jika ada tiga elemen bernilai 1, 20, 30 akan dicetak: [1,20,30] */
/* Jika tabel kosong : menulis [] */
	int i;
	if ( Neff(T) == 0 ) {
		printf("[]");
	} else {
		printf("[");
		for ( i = 1; i <= (Neff(T)-1); i++ ) {
			printf( "%d,",Elmt(T,i) );
		}
		printf("%d", Elmt( T, Neff(T) ));
		printf("]");
	}
}
/* ********** OPERATOR ARITMATIKA ********** */
/* *** Aritmatika tabel : Penjumlahan, pengurangan, perkalian, ... *** */
TabInt PlusTab (TabInt T1, TabInt T2){
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Mengirimkan  T1+T2, yaitu setiap elemen T1 dan T2 pada indeks yang sama dijumlahkan */
	TabInt T3;
	int i;
	MakeEmpty(&T3);
	Neff(T3) = Neff(T1);
	for ( i = 1; i <= Neff(T3); i++ ) {
		Elmt(T3,i) = Elmt(T1,i) + Elmt(T2,i);
	}
	return T3;
}
TabInt MinusTab (TabInt T1, TabInt T2){
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Mengirimkan T1-T2, yaitu setiap elemen T1 dikurangi elemen T2 pada indeks yang sama */
	TabInt T3;
	int i;
	MakeEmpty(&T3);
	Neff(T3) = Neff(T1);
	for ( i = 1; i <= Neff(T3); i++ ) {
		Elmt(T3,i) = Elmt(T1,i) - Elmt(T2,i);
	}
	return T3;
}
TabInt KaliTab (TabInt T1, TabInt T2){
/* Prekondisi : T1 dan T2 berukuran sama dan tidak kosong */
/* Mengirimkan T1 * T2 dengan definisi setiap elemen dengan indeks yang sama dikalikan */
	TabInt T3;
	int i;
	MakeEmpty(&T3);
	Neff(T3) = Neff(T1);
	for ( i = 1; i <= Neff(T3); i++ ) {
		Elmt(T3,i) = Elmt(T1,i) * Elmt(T2,i);
	}
	return T3;
}
TabInt KaliKons (TabInt Tin, ElType c){
/* Prekondisi : Tin tidak kosong */
/* Mengirimkan tabel dengan setiap elemen Tin dikalikan c */
	TabInt Tout;
	int i;
	MakeEmpty(&Tout);
	Neff(Tout) = Neff(Tin);
	for ( i = 1; i <= Neff(Tout); i++ ) {
		Elmt(Tout,i) = Elmt(Tin,i) * c;
	}
	return Tout;
}
/* ********** OPERATOR RELASIONAL ********** */
/* *** Operasi pembandingan tabel : < =, > *** */
boolean IsEQ (TabInt T1, TabInt T2) {
/* Mengirimkan true jika T1 sama dengan T2 yaitu jika ukuran T1 = T2 dan semua elemennya sama */
	boolean ElmtEQ = true; 
	int i = 1;
	if ( (Neff(T1) != Neff(T2) ) ) {
		return false;
	} else {
		while ( (ElmtEQ) && (i<=Neff(T1) ) ){
			if ( Elmt(T1,i) != Elmt(T2,i) ) {
				ElmtEQ = false;
			}
			i++;
		}
		return ( ElmtEQ && (Neff(T1) == Neff(T2)) ); 
	}
}
boolean IsLess (TabInt T1, TabInt T2) { 
/* Mengirimkan true jika T1 < T2, */
/* yaitu : sesuai dg analogi 'Ali' < Badu'; maka [0, 1] < [2, 3] */
	boolean IsLessElmt = false;
	int i = 1;
	while ( (Elmt(T1,i) <= Elmt(T2,i)) && (!IsLessElmt) && (i<=Neff(T1)) && (i<=Neff(T2)) ) {
		if ( (Elmt(T1,i) < Elmt(T2,i)) ) {
			IsLessElmt = true;
		} else {
			i++;
		}
	}
	if (Neff(T1)<Neff(T2)) {
		if ( (Elmt(T1,i-1)) <= Elmt(T2,i-1) ) {
			IsLessElmt = true;
		}
	}
	
	return IsLessElmt;
}
/* ********** SEARCHING ********** */
/* ***  Perhatian : Tabel boleh kosong!! *** */
IdxType Search1 (TabInt T, ElType X){
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan indeks i terkecil, dengan elemen ke-i = X */
/* Jika tidak ada, mengirimkan IdxUndef */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel T kosong */
/* Memakai skema search TANPA boolean */
	int i = 1;
	if ( Neff(T) == 0 ) {
		return IdxUndef;
	} else {
		while ( (i<=Neff(T) && (Elmt(T,i)!=X)) ) {
			i++;	
		}
		if ( Elmt(T,i) == X ) {
			return i;
		} else {
			return IdxUndef;
		}
	}
}
IdxType Search2 (TabInt T, ElType X){
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan indeks i terkecil, dengan elemen ke-i = X */
/* Jika tidak ada, mengirimkan IdxUndef */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel T kosong */
/* Memakai skema search DENGAN boolean Found */
	int i = 1;
	boolean ketemu = false;
	if ( Neff(T) == 0 ) {
		return IdxUndef;
	} else {
		while ( (i<=Neff(T) && (!ketemu)) ) {
			if ( Elmt(T,i) == X ) {
				ketemu = true;
			} else {
				i++;
			}
		}
		if ( ketemu ) {
			return i;			
		} else {
			return IdxUndef;
		}
	}

}
boolean SearchB (TabInt T, ElType X) {
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan true, jika tidak ada menghasilkan false */
/* Memakai Skema search DENGAN boolean */
	int i = 1;
	boolean ketemu = false;
	if ( Neff(T) == 0 ) {
		return ketemu;
	} else {
		while ( (i<=Neff(T) && (!ketemu) ) ) {
			if ( Elmt(T,i) == X ) {
				ketemu = true;
			} else {
				i++;
			}
		}
		return ketemu;
	}
}
boolean SearchSentinel (TabInt T, ElType X) {
/* Search apakah ada elemen tabel T yang bernilai X */
/* Jika ada, menghasilkan true, jika tidak ada menghasilkan false */
/* dengan metoda sequential search dengan sentinel */
/* Untuk sentinel, manfaatkan indeks ke-0 dalam definisi array dalam Bahasa C 
   yang tidak dipakai dalam definisi tabel */
	int i;
	Elmt(T,0) = X;
	i = Neff(T);
	while ( Elmt(T,i) != X ) {
		i--;
	}
	if ( i == 0 ) {
		return false;
	} else {
		return true;
	}
}
/* ********** NILAI EKSTREM ********** */
ElType ValMax (TabInt T) {
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan nilai maksimum tabel */
	int maks = Elmt(T,1);
	int i;
	for ( i = 2; i <= Neff(T); i++ ) {
		if ( Elmt(T,i) >= maks ) {
			maks = Elmt(T,i);
		}
	}
	return maks;
}
ElType ValMin (TabInt T){ 
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan nilai minimum tabel */
	int mins = Elmt(T,1);
	int i;
	for ( i = 2; i <= Neff(T); i++ ) {
		if ( Elmt(T,i) <= mins ) {
			mins = Elmt(T,i);
		}
	}
	return mins;
}
/* *** Mengirimkan indeks elemen bernilai ekstrem *** */
IdxType IdxMaxTab (TabInt T) {
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks i terkecil dengan nilai elemen merupakan nilai maksimum pada tabel */
	return ( Search1( T,ValMax(T) ) );
}
IdxType IdxMinTab (TabInt T) {
/* Prekondisi : Tabel T tidak kosong */
/* Mengirimkan indeks i terkecil dengan nilai elemen merupakan nilai minimum pada tabel */
	return ( Search1( T,ValMin(T) ) );
}
/* ********** OPERASI LAIN ********** */
void CopyTab (TabInt Tin, TabInt * Tout) {
/* I.S. Tin terdefinisi, Tout sembarang */
/* F.S. Tout berisi salinan dari Tin (elemen dan ukuran identik) */
/* Proses : Menyalin isi Tin ke Tout */
	int i;
	MakeEmpty(Tout);
	for ( i = 1; i <= Neff(Tin); i++ ) {
		Elmt(*Tout,i) = Elmt(Tin,i);
	}
	Neff(*Tout) = Neff(Tin);
}
TabInt InverseTab (TabInt T) {
/* Menghasilkan tabel dengan urutan tempat yang terbalik, yaitu : */
/* elemen pertama menjadi terakhir, */
/* elemen kedua menjadi elemen sebelum terakhir, dst.. */
/* Tabel kosong menghasilkan tabel kosong */
	TabInt THasil;
	MakeEmpty(&THasil);
	int i;
	Neff(THasil) = Neff(T);
	for (i=1; i<=Neff(THasil); i++) {
		Elmt(THasil,i) = Elmt(T,Neff(THasil)-i+1);
	}
	return THasil;
}
boolean IsSimetris (TabInt T) {
/* Menghasilkan true jika tabel simetrik */
/* Tabel disebut simetrik jika: */
/*      elemen pertama = elemen terakhir, */
/*      elemen kedua = elemen sebelum terakhir, dan seterusnya */
/* Tabel kosong adalah tabel simetris */
	TabInt T2;
	T2 = InverseTab(T);
	return (IsEQ(T,T2));
}
/* ********** SORTING ********** */
void MaxSortDesc (TabInt * T) {
/* I.S. T boleh kosong */
/* F.S. T elemennya terurut menurun dengan Maximum Sort */
/* Proses : mengurutkan T sehingga elemennya menurun/mengecil */
/*          tanpa menggunakan tabel kerja */
	int idxmaks;
	int i=1;
	int j;
	int temp;
	if (Neff(*T) != 0 ) {
		for ( i = 1; i<Neff(*T); i++) {
			idxmaks = i;

			for (j = i+1; j<=Neff(*T); j++) {
				if (Elmt(*T,j)>Elmt(*T,idxmaks)) {
					idxmaks = j;
				}
			}

			if (idxmaks!=i) {
				temp = Elmt(*T,idxmaks);
				Elmt(*T,idxmaks) = Elmt(*T,i);
				Elmt(*T,i) = temp;
			}

		}	
	}
}
void InsSortAsc (TabInt * T) {
/* I.S. T boleh kosong */
/* F.S. T elemennya terurut menaik dengan Insertion Sort */
/* Proses : mengurutkan T sehingga elemennya menaik/membesar */
/*          tanpa menggunakan tabel kerja */
	int i,j,temp;
	for ( i = 2; i<=Neff(*T); i++ ) {
		temp = Elmt(*T,i);
		j = i-1;
		
		while ( (j>=1) && ( Elmt(*T,j)>temp) ) {
			Elmt(*T,j+1) = Elmt(*T,j);
			j--;
		}
		
		Elmt(*T,j+1) = temp;
	} 
}
/* ********** MENAMBAH ELEMEN ********** */
/* *** Menambahkan elemen terakhir *** */
void AddAsLastEl (TabInt * T, ElType X) {
/* Proses: Menambahkan X sebagai elemen terakhir tabel */
/* I.S. Tabel T boleh kosong, tetapi tidak penuh */
/* F.S. X adalah elemen terakhir T yang baru */
	Neff(*T)++;
	Elmt(*T,Neff(*T)) = X;
}
void AddEli (TabInt * T, ElType X, IdxType i) {
/* Menambahkan X sebagai elemen ke-i tabel tanpa mengganggu kontiguitas 
   terhadap elemen yang sudah ada */
/* I.S. Tabel tidak kosong dan tidak penuh */
/*      i adalah indeks yang valid. */
/* F.S. X adalah elemen ke-i T yang baru */
/* Proses : Geser elemen ke-i+1 s.d. terakhir */
/*          Isi elemen ke-i dengan X */
	int j;
	Neff(*T)++;
	for ( j=Neff(*T); j>i; j-- ) {
		Elmt(*T,j) = Elmt(*T,j-1);
	}
	Elmt(*T,i) = X;
}
/* ********** MENGHAPUS ELEMEN ********** */
void DelLastEl (TabInt * T, ElType * X) {
/* Proses : Menghapus elemen terakhir tabel */
/* I.S. Tabel tidak kosong */
/* F.S. X adalah nilai elemen terakhir T sebelum penghapusan, */
/*      Banyaknya elemen tabel berkurang satu */
/*      Tabel T mungkin menjadi kosong */
	*X = Elmt(*T,Neff(*T));
	Neff(*T)--;
}
void DelEli (TabInt * T, IdxType i, ElType * X) {
/* Menghapus elemen ke-i tabel tanpa mengganggu kontiguitas */
/* I.S. Tabel tidak kosong, i adalah indeks efektif yang valid */
/* F.S. X adalah nilai elemen ke-i T sebelum penghapusan */
/*      Banyaknya elemen tabel berkurang satu */
/*      Tabel T mungkin menjadi kosong */
/* Proses : Geser elemen ke-i+1 s.d. elemen terakhir */
/*          Kurangi elemen efektif tabel */
	int j;
	*X = Elmt(*T,i);
	for (j=i; j<Neff(*T); j++ ) {
		Elmt(*T,j) = Elmt(*T,j+1);
	}
	Neff(*T)--;
}
/* ********** TABEL DGN ELEMEN UNIK (SETIAP ELEMEN HANYA MUNCUL 1 KALI) ********** */
void AddElUnik (TabInt * T, ElType X) {
/* Menambahkan X sebagai elemen terakhir tabel, pada tabel dengan elemen unik */
/* I.S. Tabel T boleh kosong, tetapi tidak penuh */
/*      dan semua elemennya bernilai unik, tidak terurut */
/* F.S. Jika tabel belum penuh, menambahkan X sbg elemen terakhir T, 
        jika belum ada elemen yang bernilai X. 
		Jika sudah ada elemen tabel yang bernilai X maka I.S. = F.S. 
		dan dituliskan pesan "nilai sudah ada" */
/* Proses : Cek keunikan dengan sequential search dengan sentinel */
/*          Kemudian tambahkan elemen jika belum ada */
	if (Neff(*T) == 0 ) {
		Neff(*T)++;
		Elmt(*T,Neff(*T)) = X;
	} else {
		if (SearchSentinel(*T,X)) {
			printf("nilai sudah ada\n");
		} else {
			Neff(*T)++;
			Elmt(*T,Neff(*T)) = X;
		}
	}
}
/* ********** TABEL DGN ELEMEN TERURUT MEMBESAR ********** */
IdxType SearchUrut (TabInt T, ElType X) {
/* Prekondisi: Tabel T boleh kosong. Jika tidak kosong, elemen terurut membesar. */
/* Mengirimkan indeks di mana harga X dengan indeks terkecil diketemukan */
/* Mengirimkan IdxUndef jika tidak ada elemen tabel bernilai X */
/* Menghasilkan indeks tak terdefinisi (IdxUndef) jika tabel kosong */
	int i=1;
	if (Neff(T) == 0 ) {
		return IdxUndef;
	} else {
		while ( i<=Neff(T) && (Elmt(T,i)!=X) && (X>Elmt(T,i)) ) {
			i++;
		}
		if ( Elmt(T,i) == X ) {
			return i;
		} else {
			return IdxUndef;
		}
	}
}
ElType MaxUrut (TabInt T) {
/* Prekondisi : Tabel tidak kosong, elemen terurut membesar */
/* Mengirimkan nilai maksimum pada tabel */
	return (Elmt(T,Neff(T)));
}
ElType MinUrut (TabInt T) {
/* Prekondisi : Tabel tidak kosong, elemen terurut membesar */
/* Mengirimkan nilai minimum pada tabel*/
	return (Elmt(T,IdxMin));
}
void MaxMinUrut (TabInt T, ElType * Max, ElType * Min) {
/* I.S. Tabel T tidak kosong, elemen terurut membesar */
/* F.S. Max berisi nilai maksimum T;
        Min berisi nilai minimum T */
	*Max = MaxUrut(T);
	*Min = MinUrut(T);
}
void Add1Urut (TabInt * T, ElType X) {
/* Menambahkan X tanpa mengganggu keterurutan nilai dalam tabel */
/* Nilai dalam tabel tidak harus unik. */
/* I.S. Tabel T boleh kosong, boleh penuh. */
/*      Jika tabel isi, elemennya terurut membesar. */
/* F.S. Jika tabel belum penuh, menambahkan X. */
/*      Jika tabel penuh, maka tabel tetap. */
/* Proses : Search tempat yang tepat sambil geser */
/*          Insert X pada tempat yang tepat tersebut tanpa mengganggu keterurutan */
	int i = 1;
	int j;
	if (Neff(*T) == 0 ) {
		Neff(*T)++;
		Elmt(*T,Neff(*T)) = X;
	} else if (Neff(*T) != IdxMax) {
		while ( (i<=Neff(*T) ) && (Elmt(*T,i) < X ) ) {
			i++;
		}
		Neff(*T)++;
		for ( j = Neff(*T); j>i; j-- ) {
			Elmt(*T,j) = Elmt(*T,j-1);
		}
		Elmt(*T,i) = X;
	}
}
void Del1Urut (TabInt * T, ElType X) {
/* Menghapus X yang pertama kali (pada indeks terkecil) yang ditemukan */
/* I.S. Tabel tidak kosong */
/* F.S. Jika ada elemen tabel bernilai X , */
/*      maka banyaknya elemen tabel berkurang satu. */
/*      Jika tidak ada yang bernilai X, tabel tetap. */
/*      Setelah penghapusan, elemen tabel tetap kontigu! */
/* Proses : Search indeks ke-i dengan elemen ke-i = X. */
/*          Delete jika ada. */
	int i,j;
	i = SearchUrut(*T,X);
	if (i != IdxUndef) {
		for ( j = i; j<=Neff(*T) - 1; j++ ) {
			Elmt(*T,j) = Elmt(*T,j+1);
		}
		Neff(*T)--;
	}
}
