#include <stdio.h>
#include "matriks.h"


void MakeMATRIKS (int NB, int NK, MATRIKS * M) {
/* Membentuk sebuah MATRIKS "kosong" yang siap diisi berukuran NB x NK di "ujung kiri" memori */
/* I.S. NB dan NK adalah valid untuk memori matriks yang dibuat */
/* F.S. Matriks M sesuai dengan definisi di atas terbentuk */
	NBrsEff(*M) = NB;
	NKolEff(*M) = NK;
}


/* *** Selektor "DUNIA MATRIKS" *** */
boolean IsIdxValid (int i, int j) {
/* Mengirimkan true jika i, j adalah indeks yang valid untuk matriks apa pun */
	return ( (i>=BrsMin) && (i<=BrsMax) && (j>=KolMin) && (j<=KolMax) );
}
/* *** Selektor: Untuk sebuah matriks M yang terdefinisi: *** */
indeks GetFirstIdxBrs (MATRIKS M) {
/* Mengirimkan indeks baris terkecil M */
	return (BrsMin);
}
indeks GetFirstIdxKol (MATRIKS M) {
/* Mengirimkan indeks kolom terkecil M */
	return (KolMin);
}
indeks GetLastIdxBrs (MATRIKS M) {
/* Mengirimkan indeks baris terbesar M */
	return (NBrsEff(M));
}
indeks GetLastIdxKol (MATRIKS M) {
/* Mengirimkan indeks kolom terbesar M */
	return (NKolEff(M));
}
boolean IsIdxEff (MATRIKS M, indeks i, indeks j) {
/* Mengirimkan true jika i, j adalah indeks efektif bagi M */
	return ( (i>=GetFirstIdxBrs(M)) && (i<=GetLastIdxBrs(M)) &&
			 (j>=GetFirstIdxKol(M)) && (j<=GetLastIdxKol(M)) );
}
ElType GetElmtDiagonal (MATRIKS M, indeks i) {
/* Mengirimkan elemen M(i,i) */
	return ( Elmt(M,i,i) );
}
/* ********** Assignment  MATRIKS ********** */
void CopyMATRIKS (MATRIKS MIn, MATRIKS * MHsl) {
/* Melakukan assignment MHsl  MIn */
	int i,j;
	MakeMATRIKS (NBrsEff(MIn) , NKolEff(MIn) , MHsl) ;
	for (i=GetFirstIdxBrs(MIn); i<=GetLastIdxBrs(MIn); i++) {
		for (j=GetFirstIdxKol(MIn); j<=GetLastIdxKol(MIn); j++) {
			Elmt(*MHsl,i,j) = Elmt(MIn,i,j);
		}
	}
}
/* ********** KELOMPOK BACA/TULIS ********** */
void BacaMATRIKS (MATRIKS * M, int NB, int NK) {
/* I.S. IsIdxValid(NB,NK) */
/* F.S. M terdefinisi nilai elemen efektifnya, berukuran NB x NK */
/* Proses: Melakukan MakeMATRIKS(M,NB,NK) dan mengisi nilai efektifnya */
/* Selanjutnya membaca nilai elemen per baris dan kolom */
/* Contoh: Jika NB = 3 dan NK = 3, maka contoh cara membaca isi matriks :
1 2 3
4 5 6
8 9 10
*/
	int i,j,x;
	MakeMATRIKS(NB,NK,M);

	for (i=GetFirstIdxBrs(*M); i<=GetLastIdxBrs(*M); i++) {
		for (j=GetFirstIdxKol(*M); j<=GetLastIdxKol(*M); j++) {
			scanf("%d",&x);
			Elmt(*M,i,j) = x;
		}
	}
}
void TulisMATRIKS (MATRIKS M) {
/* I.S. M terdefinisi */
/* F.S. Nilai M(i,j) ditulis ke layar per baris per kolom, masing-masing elemen per baris
   dipisahkan sebuah spasi */
/* Proses: Menulis nilai setiap elemen M ke layar dengan traversal per baris dan per kolom */
/* Contoh: menulis matriks 3x3 (ingat di akhir tiap baris, tidak ada spasi)
1 2 3
4 5 6
8 9 10
*/
	int i,j;
	for (i=GetFirstIdxBrs(M); i<GetLastIdxBrs(M); i++) {
		for (j=GetFirstIdxKol(M); j<GetLastIdxKol(M); j++) {
			printf("%d ", Elmt(M,i,j) );
		}
			printf( "%d\n",Elmt(M,i,GetLastIdxKol(M)) );
	}
	for (j=GetFirstIdxKol(M); j<GetLastIdxKol(M); j++) {
		printf( "%d ",Elmt(M,GetLastIdxBrs(M),j ) );
	}
	printf("%d",Elmt(M,GetLastIdxBrs(M),GetLastIdxKol(M) ) );
}
/* ********** KELOMPOK OPERASI ARITMATIKA TERHADAP TYPE ********** */
MATRIKS TambahMATRIKS (MATRIKS M1, MATRIKS M2) {
/* Prekondisi : M1  berukuran sama dengan M2 */
/* Mengirim hasil penjumlahan matriks: M1 + M2 */
	MATRIKS MHasil;
	int i,j;
	MakeMATRIKS(NBrsEff(M1),NKolEff(M1),&MHasil);

	for (i=GetFirstIdxBrs(MHasil); i<=GetLastIdxBrs(MHasil); i++) {
		for (j=GetFirstIdxKol(MHasil); j<=GetLastIdxKol(MHasil); j++) {
			Elmt(MHasil,i,j) = Elmt(M1,i,j) + Elmt(M2,i,j);
		}
	}

	return MHasil;
}
MATRIKS KurangMATRIKS (MATRIKS M1, MATRIKS M2) {
/* Prekondisi : M berukuran sama dengan M */
/* Mengirim hasil pengurangan matriks: salinan M1 – M2 */
	MATRIKS MHasil;
	int i,j;
	MakeMATRIKS(NBrsEff(M1),NKolEff(M1),&MHasil);

	for (i=GetFirstIdxBrs(MHasil); i<=GetLastIdxBrs(MHasil); i++) {
		for (j=GetFirstIdxKol(MHasil); j<=GetLastIdxKol(MHasil); j++) {
			Elmt(MHasil,i,j) = Elmt(M1,i,j) - Elmt(M2,i,j);
		}
	}

	return MHasil;
}
MATRIKS KaliMATRIKS (MATRIKS M1, MATRIKS M2) {
/* Prekondisi : Ukuran kolom efektif M1 = ukuran baris efektif M2 */
/* Mengirim hasil perkalian matriks: salinan M1 * M2 */
	MATRIKS MHasil;
	int i,j,k;

	MakeMATRIKS(NBrsEff(M1),NKolEff(M2), &MHasil);

	for (i=GetFirstIdxBrs(MHasil); i<=GetLastIdxBrs(MHasil); i++) {
		for (j=GetFirstIdxKol(MHasil); j<=GetLastIdxKol(MHasil); j++) {
			Elmt(MHasil,i,j) = 0;
		}
	}

	for (i=GetFirstIdxBrs(M1); i<=GetLastIdxBrs(M1); i++) {
		for (j=GetFirstIdxKol(M2); j<=GetLastIdxKol(M2); j++) {
			for (k=GetFirstIdxKol(M1); k<=GetLastIdxKol(M1); k++) {
				Elmt(MHasil,i,j) += Elmt(M1,i,k)*Elmt(M2,k,j);
			}
		}
	}

	return MHasil;
}
MATRIKS KaliKons (MATRIKS M, ElType X) {
/* Mengirim hasil perkalian setiap elemen M dengan X */
	MATRIKS MHasil;
	int i,j;
	MakeMATRIKS(NBrsEff(M),NKolEff(M),&MHasil);

	for (i=GetFirstIdxBrs(MHasil); i<=GetLastIdxBrs(MHasil); i++) {
		for (j=GetFirstIdxKol(MHasil); j<=GetLastIdxKol(MHasil); j++) {
			Elmt(MHasil,i,j) = Elmt(M,i,j) * X;
		}
	}

	return MHasil;
}
void PKaliKons (MATRIKS * M, ElType K) {
/* I.S. M terdefinisi, K terdefinisi */
/* F.S. Mengalikan setiap elemen M dengan K */
	int i,j;

	for (i=GetFirstIdxBrs(*M); i<=GetLastIdxBrs(*M); i++) {
		for (j=GetFirstIdxKol(*M); j<=GetLastIdxKol(*M); j++) {
			Elmt(*M,i,j) = Elmt(*M,i,j) * K;
		}
	}
}
/* ********** KELOMPOK OPERASI RELASIONAL TERHADAP MATRIKS ********** */
boolean EQ (MATRIKS M1, MATRIKS M2) {
/* Mengirimkan true jika M1 = M2, yaitu NBElmt(M1) = NBElmt(M2) dan */
/* untuk setiap i,j yang merupakan indeks baris dan kolom M1(i,j) = M2(i,j) */
/* Juga merupakan strong EQ karena GetFirstIdxBrs(M1) = GetFirstIdxBrs(M2)
   dan GetLastIdxKol(M1) = GetLastIdxKol(M2) */
	boolean sama;
	int i,j;
	if ( (GetLastIdxBrs(M1) != GetLastIdxBrs(M2)) || (GetLastIdxKol(M1) != GetLastIdxKol(M2)) ) {
		return false;
	} else {
		sama = true;
		i = GetFirstIdxBrs(M1);
		while (i<=GetLastIdxBrs(M1) && sama) {
			j = GetFirstIdxKol(M1);
			while (j<=GetLastIdxKol(M1) && sama ) {
				if (Elmt(M1,i,j) != Elmt(M2,i,j)) {
					sama = false;
				} else {
					j++;
				}
			}
			i++;
		}
		return sama;
	}
}
boolean NEQ (MATRIKS M1, MATRIKS M2) {
/* Mengirimkan true jika M1 tidak sama dengan M2 */
	return (!EQ(M1,M2));
}
boolean EQSize (MATRIKS M1, MATRIKS M2) {
/* Mengirimkan true jika ukuran efektif matriks M1 sama dengan ukuran efektif M2 */
/* yaitu GetBrsEff(M1) = GetNBrsEff (M2) dan GetNKolEff (M1) = GetNKolEff (M2) */
	return ( (NBrsEff(M1) == NBrsEff(M2)) && (NKolEff(M1) == NKolEff(M2)) );
}
/* ********** Operasi lain ********** */
int NBElmt (MATRIKS M) {
/* Mengirimkan banyaknya elemen M */
	return ( NBrsEff(M)*NKolEff(M) );
}
/* ********** KELOMPOK TEST TERHADAP MATRIKS ********** */
boolean IsBujurSangkar (MATRIKS M) {
/* Mengirimkan true jika M adalah matriks dg ukuran baris dan kolom sama */
	return ( NBrsEff(M) == NKolEff(M) );
}
boolean IsSimetri (MATRIKS M) {
/* Mengirimkan true jika M adalah matriks simetri : IsBujurSangkar(M)
   dan untuk setiap elemen M, M(i,j)=M(j,i) */
	boolean simetri = true;
	int i,j;
	if (IsBujurSangkar(M)) {
		i = GetFirstIdxBrs(M);
		while (i<=GetLastIdxBrs(M) && simetri) {
			j = GetFirstIdxKol(M);
			while (j<=GetLastIdxKol(M) && simetri ) {
				if (Elmt(M,i,j) != Elmt(M,j,i)) {
					simetri = false;
				} else {
					j++;
				}
			}
			i++;
		}
		return simetri;
	} else {
		return false;
	}
}
boolean IsSatuan (MATRIKS M) {
/* Mengirimkan true jika M adalah matriks satuan: IsBujurSangkar(M) dan
   setiap elemen diagonal M bernilai 1 dan elemen yang bukan diagonal bernilai 0 */
	boolean satuan = true;
	int i,j;
	if (IsBujurSangkar(M)) {
		i = GetFirstIdxBrs(M);
		while (i<=GetLastIdxBrs(M) && satuan) {
			j = GetFirstIdxKol(M);
			while (j<=GetLastIdxKol(M) && satuan ) {
				if (i == j) {
					if ( GetElmtDiagonal(M,i) != 1) {
						satuan = false;
					}
				} else {
					if ( Elmt(M,i,j) != 0 ) {
						satuan = false;
					}
				}
				j++;
			}
			i++;
		}
		return satuan;
	}
	else {
		return false;
	}
}
boolean IsSparse (MATRIKS M) {
/* Mengirimkan true jika M adalah matriks sparse: mariks “jarang” dengan definisi:
   hanya maksimal 5% dari memori matriks yang efektif bukan bernilai 0 */
	int i,j;
	int count = 0;
	for (i=GetFirstIdxBrs(M); i<=GetLastIdxBrs(M); i++) {
		for (j=GetFirstIdxKol(M); j<=GetLastIdxKol(M); j++) {
			if (Elmt(M,i,j)!=0) {
				count++;
			}
		}
	}
	if (count<= (0.05*NBrsEff(M)*NKolEff(M))) {
		return true;
	} else
		return false;
}
MATRIKS Inverse1 (MATRIKS M) {
/* Menghasilkan salinan M dengan setiap elemen "di-invers", yaitu dinegasikan (dikalikan -1) */
	MATRIKS MHasil;
	int i,j;
	MakeMATRIKS(NBrsEff(M),NKolEff(M),&MHasil);

	for (i=GetFirstIdxBrs(M); i<=GetLastIdxBrs(M); i++) {
		for (j=GetFirstIdxKol(M); j<=GetLastIdxKol(M); j++) {
			Elmt(MHasil,i,j) = Elmt(M,i,j) * (-1);
		}
	}
	return MHasil;
}
float Determinan (MATRIKS M) {
/* Prekondisi: IsBujurSangkar(M) */
/* Menghitung nilai determinan M */
	int i,j,k,x;
	boolean swap;
	int iswap,nswap = 0;
	float MHasil[101][101];
	float pengali;
	float temp;
	float hasil = 1;
	for (i=GetFirstIdxBrs(M); i<=GetLastIdxBrs(M); i++) {
		for (j=GetFirstIdxKol(M); j<=GetLastIdxKol(M); j++) {
		 	MHasil[i][j] = (float) Elmt(M,i,j);
		}
	}
	if (NBrsEff(M) == 0) {
		return 0;
	} else if (NBrsEff(M) == 1) {
			return (Elmt(M,1,1));
		} else if (NBrsEff(M) == 2 ) {
			return ( (Elmt(M,1,1) * Elmt(M,2,2)) - (Elmt(M,1,2) * Elmt(M,2,1)) );
		} else {
			for (i = GetFirstIdxBrs(M); i<GetLastIdxBrs(M); i++) {
				swap = false;
				for (j = i+1; j<=GetLastIdxBrs(M); j++) {
					x = 1;
					while ((MHasil[i][i] == 0) && (x<=GetLastIdxBrs(M)-1)) {
						for (k = 1; k <= GetLastIdxBrs(M); k++) {
              temp = MHasil[i][k];
              MHasil[i][k] = MHasil[i+x][k];
              MHasil[i+x][k] = temp;
            }
						swap = true;
            x++;
					}
					if (x == GetLastIdxBrs(M)) {
            hasil = 0;
            pengali = 1;
					} else {
            pengali = (MHasil[j][i] / MHasil[i][i]);
					}

					for (k = 1; k<=GetLastIdxBrs(M); k++) {
						MHasil[j][k] = MHasil[j][k] - (MHasil[i][k] * pengali);
					}
				}
				if (swap) {
					nswap++;
				}
			}
			for (i=GetFirstIdxBrs(M); i<=GetLastIdxBrs(M); i++) {
					hasil *= MHasil[i][i];
			}
			for (i=1; i<=nswap; i++) {
				hasil *= -1;
			}
			if (hasil == (-0)) {
				hasil = 0;
			}
			return hasil;
		}
}
void PInverse1 (MATRIKS * M) {
/* I.S. M terdefinisi */
/* F.S. M di-invers, yaitu setiap elemennya dinegasikan (dikalikan -1) */
	int i,j;
	for (i=GetFirstIdxBrs(*M); i<=GetLastIdxBrs(*M); i++) {
		for (j=GetFirstIdxKol(*M); j<=GetLastIdxKol(*M); j++) {
			Elmt(*M,i,j) = Elmt(*M,i,j) * (-1);
		}
	}
}
void Transpose (MATRIKS * M) {
/* I.S. M terdefinisi dan IsBujursangkar(M) */
/* F.S. M "di-transpose", yaitu setiap elemen M(i,j) ditukar nilainya dengan elemen M(j,i) */
	int temp;
	MATRIKS M2;
	MakeMATRIKS(NKolEff(*M),NBrsEff(*M),&M2);
	int i,j;
	for (i = GetFirstIdxKol(*M); i <= GetLastIdxKol(*M); i++){
		for (j = GetFirstIdxBrs(*M); j <= GetLastIdxBrs(*M); j++) {
				Elmt(M2,i,j) = Elmt(*M,j,i);
		}
	}
	*M = M2;
}
