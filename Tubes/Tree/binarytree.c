#include "binarytree.h"
#include <stdio.h>

int Max(int x, int y) {
/* Mengirimkan nilai yang lebih besar */
  if (x >= y) {
    return x;
  } else {
    return y;
  }
}
void CreateEmptyTree(BinTree *P) {
/* Menghasilkan Tree kosong dimana P = Nil */
  *P = Nil;
}
BinTree Tree (infotype akar, BinTree L, BinTree R) {
/* Menghasilkan sebuah pohon biner dari A, L, dan R, jika
alokasi berhasil
Menghasilkan pohon kosong (Nil) jika alokasi gagal */
  address P;
  P = Alokasi(akar);
  if (P!= Nil) {
    Left(P) = L;
    Right(P) = R;
  }
  return P;
}
void MakeTree (infotype Akar, BinTree L, BinTree R, BinTree *P) {
/*
I.S. Sembarang
F.S. Menghasilkan sebuah pohon P
Menghasilkan sebuah pohon biner P dari A, L, dan R, jika alokasi berhasil. Menghasilkan pohon P yang kosong (Nil) jika alokasi
gagal */
  *P = Alokasi(Akar);
  if (*P!=Nil) {
    Left(*P) = L;
    Right(*P) = R;
  }
}
address Alokasi (infotype X) {
/* Mengirimkan address hasil alokasi sebuah elemen X Jika alokasi berhasil, maka address tidak nil, dan
misalnya menghasilkan P, maka Info(P)=X, Left(P)=Nil, Right(P)=Nil Jika alokasi gagal, mengirimkan Nil */
  address P;
  P = (address) malloc(sizeof(address));
  if (P!= Nil) {
    Akar(P) = X;
    Left(P) = Nil;
    Right(P) = Nil;
  }
  return P;
}
void Dealokasi (address P) {
/* I.S. P terdefinisi
F.S. P dikembalikan ke sistem
Melakukan dealokasi/pengembalian address P */
  Dealokasi(P);
}
boolean IsTreeEmpty (BinTree P) {
/* Mengirimkan true jika P adalah pohon biner yang kosong */
  return (P == Nil);
}
boolean IsOneElmt (BinTree P) {
  if (!IsTreeEmpty(P)) {
    return (Left(P) == Nil && Right(P) == Nil);
  } else {
    return false;
  }
}
boolean IsUnerLeft (BinTree P) {
/* Mengirimkan true jika pohon biner tidak kosong P
adalah pohon unerleft: hanya mempunyai subpohon
kiri */
  return( Right(P) == Nil && Left(P) != Nil);
}
boolean IsUnerRight (BinTree P) {
/* Mengirimkan true jika pohon biner tidak kosong P
adalah pohon unerright: hanya mempunyai subpohon
kanan */
  return ( Left(P) == Nil && Right(P) != Nil);
}
boolean IsBiner (BinTree P) {
/* Mengirimkan true jika pohon biner tidak kosong P
adalah pohon biner: mempunyai subpohon kiri dan
subpohon kanan */
  return ( Left(P) != Nil && Right(P) != Nil);
}
void PrintPreOrder (BinTree P) {
/* I.S. Pohon P terdefinisi
F.S. Semua node pohon P sudah dicetak secara PreOrder:
akar, kiri, kanan }
Basis : Pohon kosong : tidak ada yang diproses
Rekurens : Cetak Akar(P);
cetak secara Preorder (Left(P));
cetak secara Preorder (Right(P)) */
  if (P == Nil) {

  } else {
    printf("%s",Nama(Akar(P)));
    PrintPreOrder(Left(P));
    PrintPreOrder(Right(P));
  }
}

void PrintInOrder (BinTree P) {
/* I.S. Pohon P terdefinisi
F.S. Semua node pohon P sudah dicetak secara PreOrder:
kiri,akar,kanan }
Basis : Pohon kosong : tidak ada yang diproses
Rekurens :
cetak secara Inorder (Left(P));
Cetak Akar(P);
cetak secara Inorder (Right(P)) */
  if (P == Nil) {

  } else {
    PrintPreOrder(Left(P));
    printf("%s",Nama(Akar(P)));
    PrintPreOrder(Right(P));
  }
}

void PrintPostOrder (BinTree P) {
/* I.S. Pohon P terdefinisi
F.S. Semua node pohon P sudah dicetak secara PreOrder:
kiri, kanan, akar }
Basis : Pohon kosong : tidak ada yang diproses
Rekurens :
cetak secara Postorder (Left(P));
cetak secara Postorder (Right(P))
Cetak Akar(P); */
  if (P == Nil) {

  } else {
    PrintPreOrder(Left(P));
    PrintPreOrder(Right(P));
    printf("%s",Nama(Akar(P)));
  }
}

int NbElmt(BinTree P) {
/* Pohon biner mungkin kosong. Mengirimkan jumlah elemen dari pohon */
  if (IsTreeEmpty(P)) {
    return 0;
  } else {
    return (1 + NbElmt(Left(P)) + NbElmt(Right(P)) );
  }
}
int NbDaun(BinTree P) {
/* Mengirimkan banyaknya daun pohon.
 Proses perhitungan daun menggunakan NbDaun basis 1 */
 if (Left(P) == Nil && Right(P) == Nil) {
   return 1;
 } else {
   if (Left(P) != Nil && Right(P) == Nil) {
     return (NbDaun(Left(P)));
   } else if (Left(P) == Nil && Right(P) != Nil) {
     return (NbDaun(Right(P)));
   } else if (Left(P) != Nil && Right(P) != Nil) {
     return (NbDaun(Left(P)) + NbDaun(Right(P)));
   }
 }
}
int Tinggi(BinTree R) {
/* Pohon Biner mungkin kosong.
Mengirim “height” yaitu tinggi dari pohon
Basis: Pohon kosong: tingginya nol
Rekurens: 1 + maksimum (Tinggi(Anak kiri),
Tinggi(AnakKanan)) */
  if (IsTreeEmpty(R)) {
    return 0;
  } else {
    return (1 + Max( Tinggi(Left(R)), Tinggi(Right(R)) ));
  }
}
void AddDaunTerkiri(BinTree *P, infotype X) {
/* I.S. P boleh kosong
F.S. P bertambah simpulnya,
dengan X sebagai simpul daun terkiri */
  if (IsTreeEmpty(*P)) {
    *P = Alokasi(X);
  } else {
    AddDaunTerkiri(&Left(*P),X);
  }
}
void AddDaunTerkanan(BinTree *P, infotype X) {
/* I.S. P boleh kosong
F.S. P bertambah simpulnya,
dengan X sebagai simpul daun terkanan */
  if (IsTreeEmpty(*P)) {
    *P = Alokasi(X);
  } else {
    AddDaunTerkanan(&Right(*P),X);
  }
}
void DelDaunTerkiri(BinTree *P, infotype *X) {
/* I.S. P tidak kosong
F.S. Daun terkiri P dihapus, nilai daun ditampung di X */
  address N;
  if (IsOneElmt(*P)) {
    *X = Akar(*P);
    N = *P;
    *P = Nil;
    Dealokasi(N);
  } else {
    if (IsUnerRight(*P)) {
      DelDaunTerkiri(&Right(*P),X);
    } else {
      DelDaunTerkiri(&Left(*P),X);
    }
  }
}
void DelDaunTerkanan(BinTree *P, infotype *X) {
/* I.S. P tidak kosong
F.S. Daun terkanan P dihapus, nilai daun ditampung di X */
  address N;
  if (IsOneElmt(*P)) {
    *X = Akar(*P);
    N = *P;
    *P = Nil;
    Dealokasi(N);
  } else {
    if (IsUnerLeft(*P)) {
      DelDaunTerkanan(&Left(*P),X);
    } else {
      DelDaunTerkanan(&Right(*P),X);
    }
  }
}
