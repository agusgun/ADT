#ifndef binarytree_H
#define binarytree_H

/* Kamus */
#include "boolean.h"
#include <stdlib.h>

#define Nil NULL

typedef struct{
  boolean Unlocked;
  char* Nama;
} infotype;

typedef struct tNode *address;
typedef struct tNode {
  infotype info;
  address left;
  address right;
} Node;
/* Selektor */
#define Akar(P) (P)->info
#define Left(P) (P)->left
#define Right(P) (P)->right
#define Unlocked(Info) (Info).Unlocked
#define Nama(Info) Info.Nama

/* Definisi PohonBiner : */
/* Phon Biner kosong : P = Nil */

typedef address BinTree;

void CreateEmptyTree(BinTree *P);
/* Menghasilkan Tree kosong dimana P = Nil */

BinTree Tree (infotype akar, BinTree L, BinTree R);
/* Menghasilkan sebuah pohon biner dari A, L, dan R, jika
alokasi berhasil
Menghasilkan pohon kosong (Nil) jika alokasi gagal */

void MakeTree (infotype Akar, BinTree L, BinTree R, BinTree *P);
/*I.S. Sembarang
  F.S. Menghasilkan sebuah pohon P
 Menghasilkan sebuah pohon biner P dari A, L, dan R, jika
alokasi berhasil
Menghasilkan pohon P yang kosong (Nil) jika alokasi
gagal */

address Alokasi (infotype X);
/* Mengirimkan address hasil alokasi sebuah elemen X
Jika alokasi berhasil, maka address tidak nil, dan
misalnya menghasilkan P, maka Info(P)=X, Left(P)=Nil,
Right(P)=Nil
Jika alokasi gagal, mengirimkan Nil */
void Dealokasi (address P);
/* I.S. P terdefinisi
F.S. P dikembalikan ke sistem
Melakukan dealokasi/pengembalian address P */

boolean IsTreeEmpty (BinTree P);
/* Mengirimkan true jika P adalah pohon biner yang kosong */

boolean IsOneElmt (BinTree P);
/* Mengirimkan true jika P tidak kosong dan hanya terdiri
atas 1 elemen */

boolean IsUnerLeft (BinTree P);
/* Mengirimkan true jika pohon biner tidak kosong P
adalah pohon unerleft: hanya mempunyai subpohon
kiri */

boolean IsUnerRight (BinTree P);
/* Mengirimkan true jika pohon biner tidak kosong P
adalah pohon unerright: hanya mempunyai subpohon
kanan */

boolean IsBiner (BinTree P);
/* Mengirimkan true jika pohon biner tidak kosong P
adalah pohon biner: mempunyai subpohon kiri dan
subpohon kanan */

void PrintPreOrder (BinTree P);
/* I.S. Pohon P terdefinisi
F.S. Semua node pohon P sudah dicetak secara PreOrder:
akar, kiri, kanan }
Basis : Pohon kosong : tidak ada yang diproses
Rekurens : Cetak Akar(P);
cetak secara Preorder (Left(P));
cetak secara Preorder (Right(P)) */

void PrintInOrder (BinTree P);
/* I.S. Pohon P terdefinisi
F.S. Semua node pohon P sudah dicetak secara PreOrder:
kiri,akar,kanan }
Basis : Pohon kosong : tidak ada yang diproses
Rekurens :
cetak secara Inorder (Left(P));
Cetak Akar(P);
cetak secara Inorder (Right(P)) */

void PrintPostOrder (BinTree P);
/* I.S. Pohon P terdefinisi
F.S. Semua node pohon P sudah dicetak secara PreOrder:
kiri, kanan, akar }
Basis : Pohon kosong : tidak ada yang diproses
Rekurens :
cetak secara Postorder (Left(P));
cetak secara Postorder (Right(P))
Cetak Akar(P); */

int Max(int x, int y);
/* Mengirimkan nilai yang lebih besar */

int NbElmt(BinTree P);
/* Pohon biner mungkin kosong. Mengirimkan jumlah elemen dari pohon */

int NbDaun(BinTree P);
/* Mengirimkan banyaknya daun pohon.
 Proses perhitungan daun menggunakan NbDaun basis 1 */

int Tinggi(BinTree R);
/* Pohon Biner mungkin kosong.
Tinggi(AnakKanan)) */

void AddDaunTerkiri(BinTree *P, infotype X);
/* I.S. P boleh kosong
F.S. P bertambah simpulnya,
dengan X sebagai simpul daun terkiri */

void AddDaunTerkanan(BinTree *P, infotype X);
/* I.S. P boleh kosong
F.S. P bertambah simpulnya,
dengan X sebagai simpul daun terkanan */

void DelDaunTerkiri(BinTree *P, infotype *X);
/* I.S. P tidak kosong
F.S. Daun terkiri P dihapus, nilai daun ditampung di X */

void DelDaunTerkanan(BinTree *P, infotype *X);
/* I.S. P tidak kosong
F.S. Daun terkanan P dihapus, nilai daun ditampung di X */
#endif
